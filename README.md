# README #

Backend webservice for DTU Food Phenex Tus uploader. Running Flask with Flask application with dtu-flask-tus modul in Docker.


### How do I get set up? ###
clone repo:
```bash
git clone git@bitbucket.org:genomicepidemiology/phenex-tus-server.git
```

In your docker-compose.yml configuration: 

```yalm
mongodb:
    image: mongo:3.4
    restart: always
    container_name: phenex-tus-mongodb
    volumes:
      - tusfull-db:/data/db
    environment:
      MONGO_INITDB_ROOT_USERNAME: root_username
      MONGO_INITDB_DATABASE: root_password

backend:
    image: tus-server:last
    build: ./path_to_this_folder/tus_server
    container_name: phenex-tus-backend
    restart: always
    environment:
      MONGO_TUSFULL_HOST: mongodb://host_name_of_mongo/name_of_database
    depends_on:
      - mongodb
```

For development purposes add following to your docker-compose.overrride.ylm

```yalm
backend:
  environment:
    FLASK_APP: run_app.py
    FLASK_DEBUG: 1
    FLASK_ENV: development
    MONGO_TUSFULL_USERNAME: mongo_db_username
    MONGO_TUSFULL_PASSWORD: mongo_password
  volumes:
    - ./path_to_this_folder/tus_server/tus_server:/tus_server/
  command: flask run --host=0.0.0.0
```