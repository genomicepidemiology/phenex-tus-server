# config.py
import os
import datetime

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    """
    Common configurations
    """
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'some_random_string'
    MONGODB_SETTINGS = {
        'db': 'dtu_uploader',
        'host': os.environ.get('MONGO_TUSFULL_HOST'), 
        'username': os.environ.get('MONGO_TUSFULL_USERNAME'),
        'password': os.environ.get('MONGO_TUSFULL_PASSWORD')
        }

    TUS_EXPIRATION = datetime.timedelta(days=365)

# Put any configurations here that are common across all environments
