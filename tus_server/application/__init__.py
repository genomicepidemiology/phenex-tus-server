from flask import Flask
from flask_mongoengine import MongoEngine

from dtu_flask_tus import FlaskTusExtended
from dtu_flask_tus.models import UploadModel

# local imports
from config import Config

# db variable initialization
mongo = MongoEngine()
flask_tus = FlaskTusExtended()


def create_app(config_class=Config):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config_class)

    mongo.init_app(app)
    flask_tus.init_app(app, model=UploadModel, db=mongo)

    return app
