FROM python:3.7-slim-stretch

COPY ./tus_server /tus_server
WORKDIR /tus_server
RUN pip install --upgrade pip \
    && pip install --no-cache-dir -r requirements.txt \
    && pip install -e ./dtu-flask-tus/flask-tus \
    && pip install -e ./dtu-flask-tus 
